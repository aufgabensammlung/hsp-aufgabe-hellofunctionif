﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HelloFunctionIF;

namespace HelloFunctionIF_UnitTesting
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestGrossschreibung()
        {
            // Arrange
            Program prg = new Program();

            // Act 
            var result = prg.getString(1);

            // Assert
            Assert.AreEqual(result, "HELLOFUNCTION");
        }

        [TestMethod]
        public void TestKleinschreibung1()
        {
            // Arrange
            Program prg = new Program();

            // Act 
            var result = prg.getString(2);

            // Assert
            Assert.AreEqual(result, "hellofunction");
        }

        [TestMethod]
        public void TestKleinschreibung2()
        {
            Program prg = new Program();
            var result = prg.getString(-5);
            Assert.AreEqual(result, "hellofunction");
        }

        [TestMethod]
        public void TestKleinschreibung3()
        {
            Program prg = new Program();
            var result = prg.getString(0);
            Assert.AreEqual(result, "hellofunction");
        }

    }
}
