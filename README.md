# HelloFunctionIF
Erweiterung der Aufgabe "HelloFunction" durch eine Rückgabe in Groß- oder Kleinbuchstaben für Hochsprachenprogrammierung / Mecke an der Jade Hochschule

Aufgabe: Implementieren Sie einen Algorithmus in der Methode "getString" innerhalb der Klasse "Program" Program.cs. 
* Es soll der String "HELLOFUNCTION" zurückliefert werden, wenn eine Integer Zahl 1 übergeben wird. 
* In allen andenen Fällen soll "hellofunction" zurückgeliefert werden.

## Getting Started
* Clonen Sie die Repository auf Ihren Rechner
* Öffnen Sie die Solution *.sln in Visual Studio
* Führen Sie die Implementierung in der Methode durch

Wenn Sie fertig sind, dann
* Im Projektmappen Explorer (engl. Solution Explorer) klicken Sie rechts das -Unittesting Projekt und wählen Sie "Tests ausführen"
* Starten Sie die Tests innerhalb des Test-Explorers

Diesen Ablauf können Sie auch mit Hilfe des Videos "Aufgabensammlung Einstieg" nachvollziehen
